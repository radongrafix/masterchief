module ApplicationHelper
  # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "BossDash"
    if page_title.empty?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end

  def design_style(design_css = '')
  	base_css = "theme-silver main-menu-animated dont-animate-mm-content-sm animate-mm-md animate-mm-lg"
	  if design_css.empty?
	  	base_css
	  else
		"#{design_css}"
	  end
   end
   


end
